package tools

import (
	"encoding/json"
	"fmt"
	"github.com/kataras/golog"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/database"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/utils"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
)

func RunUpdate() {
	// create a new logger
	// and make the log look good
	logger := golog.New()
	logger.SetPrefix("\x1b[32m[ADAPI]\x1b[0m\x1b[34m[UPDATE]\x1b[0m")
	// make sure the user is aware of what he is doing
	logger.Infof("Updating adapi databases.")
	logger.Infof("It is possible that all your precious data gets unusable in the process.")
	logger.Infof("Are you sure you want to proceed? There is no turning back!")
	// to make sure the user wants to proceed he has to enter the random number
	randInt := rand.Intn(256)
	logger.Infof("Please enter %d to verify that you want to proceed.", randInt)

	// get the number
	input := utils.GetText("number: ")
	// if it is not equal exit the program
	if strconv.Itoa(randInt) != input {
		logger.Infof("Exiting!")
	} else {
		// get the absolute location of the adapi directory
		adapiAbsPath, _ := filepath.Abs("./")
		logger.Infof("Looking for Schemas in %s", adapiAbsPath)
		// search for schema files
		matches, _ := filepath.Glob(adapiAbsPath + "/schemas/*.json")
		// get the content database session
		session := database.Session()
		// iterate over schema files
		session.DB("adapi").C("categories").DropCollection()
		for _, match := range matches {
			// open a schema file
			jsonFile, err := os.Open(match)
			if err != nil {
				logger.Warnf("failed to open " + match)
				fmt.Println(err)
				continue
			}
			logger.Infof("successfully opened " + match)
			// read its content
			byteValue, _ := ioutil.ReadAll(jsonFile)
			// unmarshal its content
			var result map[string]interface{}
			json.Unmarshal([]byte(byteValue), &result)
			// insert schema into content database
			session.DB("adapi").C("categories").Insert(result)
			// close the json file
			jsonFile.Close()
		}
		logger.Infof("Done")
	}
}
