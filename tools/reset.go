package tools

import (
	"encoding/json"
	"fmt"
	"github.com/kataras/golog"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/database"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/utils"
	"gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
)

func RunReset() {
	// create a new logger
	// and make the log look good
	logger := golog.New()
	logger.SetPrefix("\x1b[32m[ADAPI]\x1b[0m\x1b[34m[SETUP]\x1b[0m")
	// make sure the user is aware of what he is doing
	logger.Infof("Resetting adapi databases.")
	logger.Infof("This means that all information stored on the database will be removed.")
	logger.Infof("Are you sure you want to proceed? There is no turning back!")
	// to make sure the user wants to proceed he has to enter the random number
	randInt := rand.Intn(256)
	logger.Infof("Please enter %d to verify that you want to proceed.", randInt)

	// get the number
	input := utils.GetText("number: ")
	// if it is not equal exit the program
	if strconv.Itoa(randInt) != input {
		logger.Infof("Exiting!")
	} else {
		// if it is equal delete the tables in the authentication database
		db := database.DB()
		db.Exec("DROP TABLE IF EXISTS User;")
		db.Exec("DROP TABLE IF EXISTS UserSession;")
		db.Exec("DROP TABLE IF EXISTS AccessToken;")
		// create them again
		db.Exec("CREATE TABLE IF NOT EXISTS User (ID int auto_increment primary key, Username varchar(16) not null, PasswordHash binary(32) not null, PasswordSalt varchar(8) not null, IsDisabled tinyint(1) not null, AccessLevel int(8) not null, PrivateEmail varchar(48) not null);")
		db.Exec("CREATE TABLE IF NOT EXISTS UserSession (SessionKey binary(32) not null primary key, UserID int not null, LoginTime datetime not null, LastSeenTime datetime not null, AccessLevel int(8) not null);")
		db.Exec("CREATE TABLE IF NOT EXISTS AccessToken (Token varchar(48) not null primary key,UserID int not null, AccessLevel int(8) not null, Category varchar(16) default '' not null);")

		logger.Infof("Done resetting up the tables in authdb.")
		// get the absolute location of the adapi directory
		adapiAbsPath, _ := filepath.Abs("./")
		logger.Infof("Looking for Schemas in %s", adapiAbsPath)
		// search for schema files
		matches, _ := filepath.Glob(adapiAbsPath + "/schemas/*.json")
		// get the content database session
		session := database.Session()
		session.DB("adapi").DropDatabase()
		// iterate over schema files
		for _, match := range matches {
			// open a schema file
			jsonFile, err := os.Open(match)
			if err != nil {
				logger.Warnf("failed to open " + match)
				fmt.Println(err)
				continue
			}
			logger.Infof("successfully opened " + match)
			// read its content
			byteValue, _ := ioutil.ReadAll(jsonFile)
			// unmarshal its content
			var result map[string]interface{}
			json.Unmarshal([]byte(byteValue), &result)
			// insert schema into content database
			session.DB("adapi").C("categories").Insert(result)
			// close the json file
			jsonFile.Close()
		}
		// let the user enter the email of the new root user
		logger.Infof("Please enter the email of the new root user.")
		email := utils.GetText("email: ")

	enterPassword:
		// let the user enter the new password twice
		logger.Infof("Please enter your new password.")
		password := utils.GetTextHidden("password: ")
		logger.Infof("Please reenter your password.")
		passwordConf := utils.GetTextHidden("password: ")
		// if there not the same do it again
		if password != passwordConf {
			logger.Warnf("Passwords entered are not the same! Please retry.")
			goto enterPassword
		}
		// generate password salt an hash
		salt := database.GeneratePasswordSalt()
		hash := database.GeneratePasswordHash(password, salt)
		// create root user with password hash and salt and the new email
		root := database.User{
			ID:           0,
			Username:     "root",
			PasswordHash: hash,
			PasswordSalt: salt,
			IsDisabled:   false,
			AccessLevel:  9,
			PrivateEmail: email,
		}
		// insert the root user into the authentication database
		// afterwards the id of the root user has to be set to 0 because auto_increment only allows for indexes above 0
		// now auto_increment has to get reset
		db.Exec("INSERT INTO User (ID, Username, PasswordHash, PasswordSalt, IsDisabled, AccessLevel, PrivateEmail) VALUES (?, ?, ?, ?, ?, ?, ?);", root.ID, root.Username, root.PasswordHash, root.PasswordSalt, root.IsDisabled, root.AccessLevel, root.PrivateEmail)
		db.Exec("UPDATE User SET ID = 0 WHERE ID = 1;")
		db.Exec("ALTER TABLE User AUTO_INCREMENT = 0;")
		// insert root user into database
		database.AddEntity("users", database.Entity(bson.M{"username": root.Username, "name": "Root", "public-emails": make([]string, 0), "access-level": root.AccessLevel, "icon-link": ""}), 0)
		logger.Infof("Done")
	}
}
