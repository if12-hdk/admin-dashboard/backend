package app

import (
	"context"
	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris"
	"github.com/kataras/iris/core/router"
	icontext "github.com/kataras/iris/context"
	"github.com/kataras/iris/middleware/logger"
	"github.com/kataras/iris/middleware/recover"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/database"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/utils"
	"log"
	"os"
	"strconv"
	"time"
)

// variable to store the iris application
var App *iris.Application

// variable to store the /api 'party'
var Api router.Party
// variable to store the /user 'party'
var User router.Party
// variable to store the /log 'party'
var Log router.Party
// variable to store the /mail 'party'
var Mail router.Party

var startTime time.Time

// store the configuration
var Settings *Configuration

var Crs icontext.Handler

// run at import
// initialize variables needed to run the program
func init() {
	log.SetOutput(os.Stdout)
	// store iris app
	App = iris.New()

	// beautify the console output
	App.Logger().SetPrefix("\x1b[32m[ADAPI]\x1b[0m")
	// use logger and recover
	App.Use(recover.New())
	App.Use(logger.New())

	App.Logger().Info("initialized iris")

	// load config from file
	Settings = LoadConfig("./config/config.yml")
	App.Logger().Info("loaded configuration")
	// set log level to what is set in the config file
	App.Logger().SetLevel(Settings.LogLevel)

	database.InitContentDB()
	App.Logger().Info("connected to content database")

	database.InitAuthDB(Settings.SessionExpiration)
	App.Logger().Info("connected to authentication database")

	InitTimer()
	App.Logger().Info("started timer")

	utils.InitMail(Settings.SmtpHost, Settings.SmtpPort, Settings.SmtpUsername, Settings.SmtpPassword)
	App.Logger().Info("loaded smtp connection information")

	// create new cors object
	Crs = cors.New(cors.Options{
		// allow http requests to come from all origins
		AllowedOrigins:   []string{"*"},
		// allow all http headers
		AllowedHeaders:	  []string{"*"}, // "Authorization", "adapi-access-token", "session-key", "authenticated", "access-level"
		// allow credentials to get passed over http
		AllowCredentials: true,
		// allow http methods to be used
		AllowedMethods:	  []string{"HEAD", "GET", "POST", "DELETE", "PUT"},
	})

	// on interrupt (CTRL + C) call shutdown
	iris.RegisterOnInterrupt(func() {
		Shutdown(false, 0, "Shutting down.")
	})

	// pass a pointer of the app.Logger to the utils package (errors)
	utils.InitREL(App.Logger())

	// tell iris to allow option request method
	App.AllowMethods(iris.MethodOptions)
	App.Use(Crs)
	App.Logger().Info("app is using cors handler")

	// register '/api' and '/user'
	Api = App.Party(Settings.ApiRoot/*, Crs*/).AllowMethods(iris.MethodOptions)
	App.Logger().Info("initialized api route with cors handler")

	User = App.Party(Settings.UserRoot/*, Crs*/).AllowMethods(iris.MethodOptions)
	App.Logger().Info("initialized user route with cors handler")

	Log = App.Party(Settings.LogRoot/*, Crs*/).AllowMethods(iris.MethodOptions)
	App.Logger().Info("initialized log route with cors handler")

	Mail = App.Party(Settings.MailRoot/*, Crs*/).AllowMethods(iris.MethodOptions)
	App.Logger().Info("initialized mail route with cors handler")

	// set start time for later use
	startTime = time.Now()
}

func Run() {
	// check if root user exists
	_, errs := database.GetUserByID(0)
	// if not show message and shutdown
	if errs != nil {
		Shutdown(true, 1, "could not find root user in database. You might want to run 'adapi setup' first.")
	}
	App.Logger().Info("adapi is now running")
	// run iris app
	App.Run(iris.Addr(Settings.Host + ":" + strconv.Itoa(Settings.Port)), iris.WithConfiguration(iris.YAML("./config/config.yml")))
}

// return time since start
func UpTime() time.Duration {
	return time.Since(startTime)
}

// shutdown the app
// exit: should the application exit
// code: the code to return after shutdown
// message: message to show before shutdown
func Shutdown(exit bool, code int, message string) {
	// show error if code is not 0
	if code == 0 {
		App.Logger().Info(message)
	} else {
		App.Logger().Error(message)
	}
	timeout := 5 * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	// close databases
	database.CloseContentDB()
	database.CloseAuthDB()
	stopTimer()
	App.Logger().Infof("stopped timer!")
	// close all hosts
	App.Logger().Infof("shutting down!")
	App.Shutdown(ctx)
	if exit {
		os.Exit(code)
	}
}