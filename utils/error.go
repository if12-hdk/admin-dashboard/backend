package utils

import (
	"github.com/kataras/golog"
	"github.com/kataras/iris"
	"gopkg.in/mgo.v2/bson"
)

// type to hold a error together with a message and a error code (see composition at the bottom)
type AdapiError struct {
	Error   error
	Message string
	Code    int
}

// a list of adapi errors
type ErrorList struct {
	Errors []AdapiError
}

// a type to hold a pointer to the request context and the default status code which gets set when no error occurred
// it is used set the status of the http request when Status is called and return the errors to the user
// also it logs the error in the output
type Response struct {
	defaultStatus 	int
	ctx 			*iris.Context
}

var logger *golog.Logger

// a function to create one adapi error
func Error(err error, message string, code int) AdapiError {
	return AdapiError{Message: message, Error:err, Code:code}
}

// a function to create a adapi error list with one error
func CreateErrorList(err error, message string, code int) *ErrorList {
	return &ErrorList{[]AdapiError{AdapiError{Message: message, Error:err, Code:code}}}
}

// a method to add one adapi error to a adapi error list
func (errorList *ErrorList) Add(error AdapiError) {
	// append to adapi error list
	errorList.Errors = append(errorList.Errors, error)
}

// returns a json representation of the errors
func (errorList ErrorList) JSON() bson.M {
	return bson.M{"Errors":errorList.Errors}
}

// returns the http status code of the worst error in the list
func (errorList ErrorList) GetCodes() ([]int, int) {
	// hold current worst http status code
	code := 0
	// a list of all adapi error codes to print them in output
	var codes []int
	// iterate over all errors in the list
	for _, err := range errorList.Errors {
		// append the error code to codes
		codes = append(codes, err.Code)
		// if the last 3 digits of the error code (http status code) are higher (worse) than in code replace code
		if c := LastNDigits(err.Code, 3); c > code {
			code = c
		}
	}
	// return codes and code
	return codes, code
}

// initREL takes the pointer to the app logger one time so it is not necessary to pass it every time
func InitREL(l *golog.Logger) {
	logger = l
}

func NewREL(ctx *iris.Context, defaultStatus int) *Response {
	// create a response object from the default status code and an pointer to a context object
	// set the default status code in case Status() doesn't get called
	(*ctx).StatusCode(defaultStatus)
	// return response object
	return &Response{defaultStatus: defaultStatus, ctx:ctx}
}

func (response Response) Status(errs *ErrorList) {
	// if no errors occurred set default status code
	// if there were errors
	if errs != nil {
		// add the errors in json form to the http response
		response.Write(errs.JSON())
		// get the codes and the worst http status code
		codes, code := errs.GetCodes()
		// set the http status code
		(*response.ctx).StatusCode(code)
		// log the request in the output
		logger.Infof("A request from \x1b[33m%s\x1b[0m \x1b[37m[%s]\x1b[0m produced the following error(s): \x1b[31m%d\x1b[0m!", (*response.ctx).Request().RemoteAddr, (*response.ctx).Request().UserAgent(), codes)
		return
	}
	(*response.ctx).StatusCode(response.defaultStatus)
}

func (response Response) Write(v interface{}) {
	if _, err := (*response.ctx).JSON(v); err != nil {
		logger.Errorf("An error occurred while writing a json response for \x1b[33m%s\x1b[0m \x1b[37m[%s]\x1b[0m !", (*response.ctx).Request().RemoteAddr, (*response.ctx).Request().UserAgent())
	}
}


/*
 * Error Code:
 *
 * Example: 101002500
 *
 * 101 00 2  500
 *  |  |  |   |
 *  |  |  |  http status code
 *  |  | error category
 *  | index of error in procedure
 * procedure code
 *
 */

 /*
  * error category:
  * 0 : no category
  * 1 : type error
  * 2 : authentication error
  * 3 : internal error
  * 4 : database error
  * 5 : not allowed
  * 6 : input error
  */