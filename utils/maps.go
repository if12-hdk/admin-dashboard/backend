package utils

// swap key and value in an map
func SwapMap(m map[string]string) map[string]string {
	// create new map
	newMap := map[string]string{}
	// iterate over the whole original map
	for item := range m {
		// swap key and value
		newMap[m[item]] = item
	}
	// return new map
	return newMap
}
