package routes

import (
	"github.com/kataras/iris"
	"github.com/kataras/iris/core/errors"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/app"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/database"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/middleware"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/utils"
	"gopkg.in/mgo.v2/bson"
	"reflect"
	"strconv"
)

type adapiData struct {
	From	int					`json:"from"`
	Count 	int					`json:"count"`
	Sort 	[]string			`json:"sort"`
	Find 	bson.M				`json:"find"`
	Data 	[]database.Entity	`json:"data"`
}

// a function which is called from the app package
// it calls register routes functions for specific groups
func RegisterRoutes() {
	RegisterGeneral()
	RegisterUserManagement()
	RegisterLogs()
	RegisterError()
	RegisterMail()
}

// register all routes related to content and the login and logout route
func RegisterGeneral() {
	// get general information about the server status
	app.Api.Get("/", handleGetPartyRoot)
	// get stats for a specific category
	app.Api.Get("/{category}/stats", middleware.NeedsAccessLevel(2), handleGetCategoryStats)
	// get all entities of a category
	app.Api.Post("/{category}", middleware.NeedsAccessLevel(3), handleGetCategoryList)
	// get object with id
	app.Api.Get("/id/{id}", middleware.NeedsAccessLevel(1), handleGetId)

	// post a new entity to a category
	app.Api.Post("/{category}/create", middleware.NeedsAccessLevel(4), handlePostCategory)
	// post a new entity 'count' times to a category
	app.Api.Post("/{category}/create/{count}", middleware.NeedsAccessLevel(4), handlePostCategoryMultiple)

	// update a entity in a category
	app.Api.Put("/{category}", middleware.NeedsAccessLevel(5), handlePutCategorySearch)

	// delete a entity from a category
	app.Api.Delete("/{category}/{key}/{value}", middleware.NeedsAccessLevel(6), handleDeleteCategorySearch)

	// login
	app.App.Post("/login"/*, app.Crs*/, handleLogin)
	//logout
	app.App.Get("/logout"/*, app.Crs*/, handleLogout)
}


// GET
// description: return connection information
// path: /
// procedure code : 100
func handleGetPartyRoot(ctx iris.Context) {
	// create new response error list
	response := utils.NewREL(&ctx, 200)
	response.Write(iris.Map{
		"status": 	"online",
		"host":     app.Settings.Host,
		"port": 	strconv.Itoa(app.Settings.Port),
		"uptime": 	app.UpTime(),
	})
}

// GET
// description: return statistics about a category
// path: /{category}/stats
// procedure code : 101
func handleGetCategoryStats(ctx iris.Context) {
	// create new response error list
	response := utils.NewREL(&ctx, 200)
	// check whether the category exists
	exists, errs := database.CategoryExists(ctx.Params().Get("category"))
	if errs != nil {
		// return http status and errors
		response.Status(errs)
		}
	if exists {
		// return the count of entities
		stats, errs := database.GetCategoryStats(ctx.Params().Get("category"))
		if errs != nil {
			response.Status(errs)
			return
		}
		// write to response
		response.Write(stats)
	}
}

// POST
// description: return all documents in category that match the search parameters
// path: /{category}
// procedure code : 110
func handleGetCategoryList(ctx iris.Context) {
	// create response error list
	response := utils.NewREL(&ctx, 200)
	// check whether category exists
	exists, errs := database.CategoryExists(ctx.Params().Get("category"))
	if errs != nil {
		response.Status(errs)
		return
	}
	var data adapiData
	// read sorting data from request
	if err := ctx.ReadJSON(&data); err != nil {
		response.Status(utils.CreateErrorList(err, "you have to supply 'from' as a int", 113001400))
		return
	}
	// make sure that the sorting values are in a valid range
	if errs := processData(&data); errs != nil {
		response.Status(errs)
		return
	}
	// if it exists get all entities
	if exists {
		entities, errs := database.GetFromNEntitiesSorted(ctx.Params().Get("category"), data.Find, data.From, data.Count, data.Sort...)
		if errs != nil {
			response.Status(errs)
		} else {
			// write to response
			response.Write(entities)
		}
	}
}


// GET
// description: return all matching documents in category
// path: /id/{id}
// procedure code : 120
func handleGetId(ctx iris.Context) {
	// create new response error list
	response := utils.NewREL(&ctx, 200)
	// get id from url
	adid := ctx.Params().Get("id")
	// get the category from the adid
	category, errs := database.GetCategoryFromADID(adid)
	// check whether category exists
	exists, errs := database.CategoryExists(category)
	if errs != nil {
		response.Status(errs)
	}
	// if the category exists get the object id from the adid
	if exists {
		id, errs := database.GetIdFromADID(adid)
		if errs != nil {
			response.Status(errs)
			return
		}
		// if no error occurred get the object with id from the category
		entity, errs := database.GetFromNEntities(category, bson.M{"_id": id}, 0, 1)
		if errs != nil {
			response.Status(errs)
		} else {
			// write to response
			response.Write(entity)
		}
	}
}

// TODO:DONE! restrict access to user, adid and templates
// POST
// description: add document to the category
// path: /{category}/create
// procedure code : 130
func handlePostCategory(ctx iris.Context) {
	// create response error list
	response := utils.NewREL(&ctx, 201)
	// check whether category exists
	exists, errs := database.CategoryExists(ctx.Params().Get("category"))
	if errs != nil {
		response.Status(errs)
	}
	// check if the category the user wants to add something to is not users categories or adid
	if ctx.Params().Get("category") == "users" || ctx.Params().Get("category") == "categories" || ctx.Params().Get("category") == "adid" {
		exists = false
		response.Status(utils.CreateErrorList(errors.New("not allowed"), "access to users, categories and adid is not allowed", 130005403))
	}
	// if the category exists
	if exists {
		var data adapiData
		err := ctx.ReadJSON(&data)
		if err != nil {
			response.Status(utils.CreateErrorList(err, "can't get data from request", 130012401))
			return
		}
		entities := data.Data
		if entities == nil {
			response.Status(utils.CreateErrorList(err, "can't get entities from request", 130022401))
			return
		}
		// if it does get the user id from context
		userID, err :=  ctx.Values().GetInt("userID")
		if err != nil {
			response.Status(utils.CreateErrorList(err, "can't get user. You might want to authenticate first.", 130032401))
			return
		}
		// add the entity on the category and add the user id as owner
		var errs *utils.ErrorList
		// loop trough all entities to be added
		for _, entity := range entities {
			// add them to the database
			errs = database.AddEntity(ctx.Params().Get("category"), entity, userID)
			// if an error occurs stop and return the error
			if errs != nil {
				break
			}
		}
		if errs != nil {
			response.Status(errs)
		}
	}
}

// TODO:DONE! restrict access to user, adid and templates
// POST
// description: add document to the category
// path: /{category}/create/{count}
// procedure code : 131
func handlePostCategoryMultiple(ctx iris.Context) {
	// create response error list
	response := utils.NewREL(&ctx, 201)
	// check whether category exists
	exists, errs := database.CategoryExists(ctx.Params().Get("category"))
	if errs != nil {
		response.Status(errs)
	}
	// check if the category the user wants to add something to is not users categories or adid
	if ctx.Params().Get("category") == "users" || ctx.Params().Get("category") == "categories" || ctx.Params().Get("category") == "adid" {
		exists = false
		response.Status(utils.CreateErrorList(errors.New("not allowed"), "access to users, categories and adid is not allowed", 131005403))
	}
	// if the category exists
	if exists {
		// get the amount of new entities
		count, err :=  ctx.Params().GetInt("count")
		if err != nil || count == 0 {
			response.Status(utils.CreateErrorList(err, "could not get amount of new entities or count is 0", 131012401))
			return
		}
		// read the data from the request
		var data adapiData
		err = ctx.ReadJSON(&data)
		if err != nil {
			response.Status(utils.CreateErrorList(err, "can't get data from request", 131022401))
			return
		}
		// get the entity from data
		entity := data.Data[0]
		if entity == nil {
			response.Status(utils.CreateErrorList(err, "can't get entities from request", 131032402))
			return
		}
		// if it does get the user id from context
		userID, err :=  ctx.Values().GetInt("userID")
		if err != nil {
			response.Status(utils.CreateErrorList(err, "can't get user. You might want to authenticate first.", 131042401))
			return
		}
		// add the entity on the category and add the user id as owner
		var errs *utils.ErrorList
		// create the entity 'count' times
		for i := 0; i < count; i++ {
			errs := database.AddEntity(ctx.Params().Get("category"), entity, userID)
			// if an error occurs stop and return the error
			if errs != nil {
				break
			}
		}
		if errs != nil {
			response.Status(errs)
		}
	}
}


// PUT
// description: update document in category
// path: /{category}
// procedure code : 140
func handlePutCategorySearch(ctx iris.Context) {
	// create new response error list
	response := utils.NewREL(&ctx, 204)
	// check whether category exists
	exists, errs := database.CategoryExists(ctx.Params().Get("category"))
	if errs != nil {
		response.Status(errs)
	} else {
		// check if the category the user wants to add something to is not users categories or adid
		if ctx.Params().Get("category") == "users" || ctx.Params().Get("category") == "categories" || ctx.Params().Get("category") == "adid" {
			exists = false
			response.Status(utils.CreateErrorList(errors.New("not allowed"), "access to users, categories and adid is not allowed", 140005403))
		}
		// if the category exists
		if exists {
			// get the data from the request
			var data adapiData
			err := ctx.ReadJSON(&data)
			if err != nil {
				response.Status(utils.CreateErrorList(err, "can't get data from request", 140012401))
				return
			}
			// get the entity from data
			entity := data.Data[0]
			if entity == nil {
				response.Status(utils.CreateErrorList(err, "can't get entities from request", 140022402))
				return
			}
			// update the entity in the database
			errs = database.UpdateEntity(ctx.Params().Get("category"), data.Find, entity)
			if errs != nil {
				response.Status(errs)
			}
		} else {
			response.Status(utils.CreateErrorList(errors.New("internal error"), "this should not have happened.", 140033500))
		}
	}
}


// DELETE
// description: remove document from the category
// path: /{category}/{key}/{value}
// procedure code : 150
func handleDeleteCategorySearch(ctx iris.Context) {
	// create new response error list
	response := utils.NewREL(&ctx, 201)
	// check whether category exists
	exists, errs := database.CategoryExists(ctx.Params().Get("category"))
	if errs != nil {
		response.Status(errs)
	}
	// if the category exists
	if exists {
		// delete the entity which matches the key value pair
		errs := database.DeleteEntity(ctx.Params().Get("category"), bson.M{ctx.Params().Get("key"): ctx.Params().Get("value")})
		if errs != nil {
			response.Status(errs)
		}
	}
}


// POST
// description: log user in
// path: /login
// procedure code : 160
func handleLogin(ctx iris.Context) {
	// create new response error list
	response := utils.NewREL(&ctx, 200)
	// attempt to log user in
	errs := middleware.Login(ctx)
	if errs != nil {
		response.Status(errs)
	}
}


// GET
// description: log user out
// path: /logout
// procedure code : 170
func handleLogout(ctx iris.Context) {
	// create new response error list
	response := utils.NewREL(&ctx, 200)
	// attempt to log user out
	errs := middleware.Logout(ctx)
	if errs != nil {
		response.Status(errs)
	}
}


// procedure code : 180
func processData(data *adapiData) *utils.ErrorList {
	// limit from and count and check if it is the right format
	if data.From < 0 {
		data.From = 0
	} else {
		if reflect.TypeOf(data.From).Kind() != reflect.Int {
			return utils.CreateErrorList(errors.New("type error"), "you have to supply 'from' as a int", 180001400)
		}
	}
	if data.Count <= 0 {
		data.Count = 10
	} else {
		if reflect.TypeOf(data.Count).Kind() != reflect.Int {
			return utils.CreateErrorList(errors.New("type error"), "you have to supply 'count' as a int", 180011400)
		}
	}
	// if sort is nil set it to a string array containing _id
	if data.Sort == nil {
		data.Sort = []string{"_id"}
	} else if !(reflect.TypeOf(data.Sort).Kind() == reflect.Slice && reflect.TypeOf(data.Sort).Elem().Kind() == reflect.String) {
		// if it is not the right type return error
		return utils.CreateErrorList(errors.New("type error"), "you have to supply 'sort' as a string slice", 180021400)
	}
	return nil
}