package routes

import (
	"github.com/kataras/iris"
	"gitlab.com/helmholtzschule/admin-dashboard/adapi/app"
	"gopkg.in/mgo.v2/bson"
)

func RegisterError() {
	app.App.OnErrorCode(iris.StatusNotFound, app.Crs, handleNotFound)
}



func handleNotFound(ctx iris.Context) {
	ctx.JSON(bson.M{"Errors": []bson.M{{
		"Message": "you have to supply 'from' as a int",
		"Code": 900006404,
	}}})
}