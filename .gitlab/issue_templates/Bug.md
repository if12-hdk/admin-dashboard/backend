~Bug
> **Please read this!**  
>
> Please use a descriptive Title.  
> Before opening a new issue, make sure to search for [possible duplicates](https://gitlab.com/helmholtzschule/admin-dashboard/adapi/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Bug)!

### Summary  
> Summarize the bug encountered.  

### Steps to reproduce  
> How one can reproduce the issue?  

### current behavior  
> What actually happens  

### expected behavior  
> What you should see instead  

### Relevant logs and/or screenshots  
<details>
<summary>logs and/or screenshots</summary>
<pre>

```
paste logs here / delete if not needed  
```

</pre>
</details>

### Possible fixes  
> If you can, link to the line of code that might be responsible for the problem.  

> ## need help writing Markdown?  
>
> [gitlab markdown reference](https://gitlab.com/help/user/markdown#code-and-syntax-highlighting)  
> [create link to line of code](https://help.github.com/articles/getting-permanent-links-to-files/)  
>
> {- don't forget to delete all lines preceded by a '>'. -}  

/label ~Bug