~Documentation
> **Please read this!**  
>
> Please use a descriptive Title.  
> Before opening a new issue, make sure to search for [possible duplicates](https://gitlab.com/helmholtzschule/admin-dashboard/adapi/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Documentation)!

### Summary  
> Summarize what's wrong / missing.  

### Changes
> make a suggestion what you would change / add

> ## need help writing Markdown?  
>
> [gitlab markdown reference](https://gitlab.com/help/user/markdown#code-and-syntax-highlighting)  
> [create link to line of code](https://help.github.com/articles/getting-permanent-links-to-files/)  
>
> {- don't forget to delete all lines preceded by a '>'. -}  

/label ~Documentation